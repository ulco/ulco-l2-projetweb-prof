<?php

namespace model;

class AccountModel {

  static function signin($firstname, $lastname, $mail, $password)
  {
    $db = \model\Model::connect();
    $sql = "INSERT INTO account (firstname, lastname, mail, password)
            VALUES (?, ?, ?, ?)";
    $sth = $db->prepare($sql);
    return $sth->execute(array($firstname, $lastname, $mail, $password));
  }

  static function login($mail, $password)
  {
    $db = \model\Model::connect();
    $sql = "SELECT id, firstname, lastname, mail
            FROM account
            WHERE mail = ? AND password = ?";
    $sth = $db->prepare($sql);
    $sth->execute(array($mail, $password));
    return $sth->fetch();
  }

  static function update($id, $firstname, $lastname, $mail)
  {
    $db = \model\Model::connect();
    $sql = "UPDATE account
            SET firstname = ?, lastname = ?, mail = ?
            WHERE id = ?";
    $sth = $db->prepare($sql);
    return $sth->execute(array($firstname, $lastname, $mail, $id));
  }

}