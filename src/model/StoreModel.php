<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";

    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function listProducts(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT c.name as category, p.id, p.name, p.price, p.image
            FROM product AS p
            INNER JOIN category AS c ON p.category = c.id";

    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function infoProduct(int $id)
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT c.name as category, p.id, p.name, p.price, p.image,
                   p.image_alt1, p.image_alt2, p.image_alt3, p.spec
            FROM product AS p
            INNER JOIN category AS c ON p.category = c.id
            WHERE p.id = ?";

    // Exécution de la requête
    $sth = $db->prepare($sql);
    $sth->execute(array($id));

    // Retourner les résultats (type array)
    return $sth->fetch();
  }

  static function listProducts2($search, $filter, $sort): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT c.name as category, p.id, p.name, p.price, p.image
            FROM product AS p
            INNER JOIN category AS c ON p.category = c.id
            WHERE p.name LIKE ?";

    // Si besoin de la categorie il faut concaténer...
    if (!empty($filter)) {
        $sql .= " AND c.name REGEXP '" . $filter . "'";
    }

    // Si besoin de ORDER BY il faut concaténer...
    if (!empty($sort)) {
      $sql .= " ORDER BY p.price " . $sort;
    }

    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute(array('%'.$search.'%'));

    // Retourner les résulttats (type array)
    return $req->fetchAll();
  }

}