<?php

namespace model;

class CommentModel {

  static function insertComment($content, $product, $account)
  {
    var_dump($content, $product, $account);
    $db = \model\Model::connect();
    $sql = "INSERT INTO comment (content, date, id_product, id_account)
            VALUES (?, NOW(), ?, ?)";
    $sth = $db->prepare($sql);
    return $sth->execute(array($content, $product, $account));
  }

  static function listComments($product): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT c.id, c.content, a.firstname, a.lastname
            FROM comment AS c
            INNER JOIN account AS a ON c.id_account = a.id
            WHERE c.id_product = ?";

    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute(array($product));

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

}