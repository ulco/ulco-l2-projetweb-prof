<?php

namespace controller;

class AccountController {

  static function redirect($location, $status=""): void
  {
    // URL
    $location = "Location: " . $location;

    // Statut
    if (!empty($status)) {
      $location .= "?status=" . $status;
    }

    // Redirection
    header($location);
    exit();
  }

  static function exitIfLoggedIn(): void
  {
    // Quitter si le visiteur est déjà connecté
    if (isset($_SESSION["user"])) {
      AccountController::redirect("/store");
    }
  }

  static function exitIfLoggedOut(): void
  {
    // Quitter si le visiteur n'est pas connecté
    if (!isset($_SESSION["user"])) {
      AccountController::redirect("/account");
    }
  }

  public function account(): void
  {
    // Quitter si le visiteur est déjà connecté
    AccountController::exitIfLoggedIn();

    // Variables transmises à la vue
    $params = [
      "module" => "account.php",
      "title"  => "Compte",
    ];

    // Faire le rendu de la vue "src/view/template.php"
    \view\Template::render($params);
  }

  public function signin(): void
  {
    // Quitter si le visiteur est déjà connecté
    AccountController::exitIfLoggedIn();

    // TODO: vérifier que les infos sont safe / respectent les contraintes
    // Communications avec la base de données
    $res = \model\AccountModel::signin(
      $_POST["userfirstname"], $_POST["userlastname"],
      $_POST["usermail"], $_POST["userpass"]
    );

    // Variables transmises à la vue
    $params = [
      "module" => "account.php",
      "title"  => "Compte",
    ];

    // Rediriger avec statut dans l'URL
    AccountController::redirect("/account", "signin_success");
  }

  public function login(): void
  {
    // Quitter si le visiteur est déjà connecté
    AccountController::exitIfLoggedIn();

    // Communications avec la base de données
    $res = \model\AccountModel::login($_POST["usermail"], $_POST["userpass"]);

    // Si les informations de connexion sont correctes
    if ($res != null) {
      // Modification de la session
      $_SESSION["user"] = array(
        "id" => $res["id"],
        "firstname" => $res["firstname"],
        "lastname" => $res["lastname"],
        "mail" => $res["mail"]
      );
      // Rediriger avec statut dans l'URL
      AccountController::redirect("/account", "login_success");
    }
    else {
      // Rediriger avec statut dans l'URL
      AccountController::redirect("/account", "login_fail");
    }
  }

  public function logout(): void
  {
    // Détriure la session
    session_destroy();

    // Rediriger avec statut dans l'URL
    AccountController::redirect("/account", "logout");
  }

  public function infos(): void
  {
    // Quitter si le visiteur n'est pas connecté
    AccountController::exitIfLoggedOut();

    // Variables transmises à la vue
    $params = [
      "module" => "infos.php",
      "title"  => "Informations du compte",
    ];

    // Faire le rendu de la vue "src/view/template.php"
    \view\Template::render($params);
  }

  public function update(): void
  {
    // Quitter si le visiteur n'est pas connecté
    AccountController::exitIfLoggedOut();

    // Mise à jour des infos personnelles
    $res = \model\AccountModel::update($_SESSION["user"]["id"],
                                       $_POST["firstname"],
                                       $_POST["lastname"],
                                       $_POST["mail"]);

    // Mise à jour de la session
    $_SESSION["user"]["firstname"] = $_POST["firstname"];
    $_SESSION["user"]["lastname"] = $_POST["lastname"];
    $_SESSION["user"]["mail"] = $_POST["mail"];

    // Redirection
    AccountController::redirect("/account/infos", "ok");
  }

}