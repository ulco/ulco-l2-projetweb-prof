<?php

namespace controller;

class CommentController {

  public function comment(): void
  {
    // Quitter si le visiteur n'est pas connecté
    if (!isset($_SESSION["user"])) {
      header("Location: /account");
      exit();
    }

    \model\CommentModel::insertComment(
      htmlspecialchars($_POST["content"]),
      htmlspecialchars($_POST["product"]),
      $_SESSION["user"]["id"]
    );
    
    // Redirection
    header("Location: /store/" . $_POST["product"]);
    exit();
  }

}