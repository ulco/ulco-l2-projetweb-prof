<?php

namespace controller;

class IndexController {

  public function index(): void
  {
    // Variables à transmettre à la vue
    $params = [
      "module" => "home.php",
      "title"  => "Home",
    ];

    // Faire le rendu de la vue "src/view/template.php"
    \view\Template::render($params);
  }

}