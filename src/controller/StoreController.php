<?php

namespace controller;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $list = \model\StoreModel::listProducts();

    // Variables transmises à la vue
    $params = array(
      "module" => "store.php",
      "title" => "Store",
      "categories" => $categories,
      "list" => $list
    );

    // Faire le rendu de la vue "src/view/template.php"
    \view\Template::render($params);
  }

  public function product(int $id): void
  {
    // Communications avec la base de données
    $product = \model\StoreModel::infoProduct($id);
    $comments = \model\CommentModel::listComments($id);

    // Si le produit n'existe pas
    if ($product == null) {
      header("Location: /store");
      exit();
    }

    // Variables transmises à la vue
    $params = [
      "module"   => "product.php",
      "title"    => "Produit " . $id,
      "product"  => $product,
      "comments" => $comments
    ];

    // Faire le rendu de la vue "src/view/template.php"
    \view\Template::render($params);
  }

  public function search(): void
  {
    // Extract $_POST
    $search = isset($_POST["search"]) ? $_POST["search"] : "";
    $filter = isset($_POST["category"]) ? join("|", $_POST["category"]) : "";
    $sort = isset($_POST["sort"]) ? $_POST["sort"] : "";

    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $list = \model\StoreModel::listProducts2($search, $filter, $sort);

    // Variables transmises à la vue
    $params = array(
      "module" => "store.php",
      "title" => "Store",
      "categories" => $categories,
      "list" => $list
    );

    // Faire le rendu de la vue "src/view/template.php"
    \view\Template::render($params);
  }
}