<?php

namespace controller;

class CartController {

  public function cart(): void
  {
    // Définition du panier dans la session s'il n'existe pas déjà
    if (!isset($_SESSION["cart"]))
      $_SESSION["cart"] = array();

    // Calcul du prix total
    $total = 0;
    foreach ($_SESSION["cart"] as $product)
      $total += intval($product["quantity"]) * intval($product["price"]);

    // Variables à transmettre à la vue
    $params = [
      "module" => "cart.php",
      "title"  => "Panier",
      "cart" => $_SESSION["cart"],
      "total" => $total
    ];

    // Faire le rendu de la vue "src/view/template.php"
    \view\Template::render($params);
  }

  public function update(): void
  {
    // Définition du panier dans la session
    if (!isset($_SESSION["cart"]))
      $_SESSION["cart"] = array();

    // Le produit existe déjà dans le panier
    if (isset($_SESSION["cart"][$_POST["id"]]))
      $_SESSION["cart"][$_POST["id"]]["quantity"] += $_POST["quantity"];
    
    // Le produit n'existe pas encore dans le panier
    else
      $_SESSION["cart"][$_POST["id"]] = array(
        "name" => $_POST["name"],
        "price" => $_POST["price"],
        "image" => $_POST["image"],
        "category" => $_POST["category"],
        "quantity" => $_POST["quantity"],
      );

    // Redirection
    header("Location: /cart");
    exit();
  }

}