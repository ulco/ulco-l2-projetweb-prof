<?php
  if (isset($_GET["status"])) {
    if ($_GET["status"] == 'ok') {
      echo '<div style="margin:32px 32px 0" class="box info">Tes informations personnelles ont été mises à jour !</div>';
    }
  }
?>

<div id="infos">

<h2>Informations du compte</h2>

<h3>Informations personnelles</h3>

<form method="post" action="/account/update">
  <table>
    <tr>
      <td>Prénom</td>
      <td><input type="text" name="firstname" value="<?= $_SESSION["user"]["firstname"] ?>" /></td>
    </tr>
    <tr>
      <td>Nom</td>
      <td><input type="text" name="lastname" value="<?= $_SESSION["user"]["lastname"] ?>" /></td>
    </tr>
    <tr>
      <td>Adresse mail</td>
      <td><input type="text" name="mail" value="<?= $_SESSION["user"]["mail"] ?>" /></td>
    </tr>
    <tr>
      <td colspan="2">
        <input type="submit" value="Modifier mes informations" />
      </td>
    </tr>
  </table>
</form>

<h3>Commandes</h3>

<p>
  Tu n'as pas de commande en cours.
</p>

</div>
