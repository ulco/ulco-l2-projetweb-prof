<div id="product">

<div>

  <div class="product-images">
    <img id="splash" src="/public/images/<?= $params["product"]["image"] ?>" />
    <div class="product-miniatures">
      <div>
        <img src="/public/images/<?= $params["product"]["image"] ?>" />
      </div>
      <div>
        <img src="/public/images/<?= $params["product"]["image_alt1"] ?>" />
      </div>
      <div>
        <img src="/public/images/<?= $params["product"]["image_alt2"] ?>" />
      </div>
      <div>
        <img src="/public/images/<?= $params["product"]["image_alt3"] ?>" />
      </div>
    </div>
  </div>

  <div class="product-infos">
    <p class="product-category">
      <?= $params["product"]["category"] ?>
    </p>
    <h1><?= $params["product"]["name"] ?></h1>
    <p class="product-price">
      <?= $params["product"]["price"] ?>€
    </p>
    <form action="/cart/update" method="post">
      <button id="btnM" type="button">-</button>
      <button id="btnQ" type="button">1</button>
      <button id="btnP" type="button">+</button>
      <input type="hidden" name="id" value="<?= $params["product"]["id"] ?>" />
      <input type="hidden" name="image" value="<?= $params["product"]["image"] ?>" />
      <input type="hidden" name="name" value="<?= $params["product"]["name"] ?>" />
      <input type="hidden" name="category" value="<?= $params["product"]["category"] ?>" />
      <input type="hidden" name="price" value="<?= $params["product"]["price"] ?>" />
      <input type="hidden" id="btnQ2" name="quantity" value="1" />
      <input type="submit" value="Ajouter au panier" />
    </form>
    <p id="overQ" class="box error">
      Quantité maximale autorisée !
    </p>
  </div>

</div>

<div>
  <div class="product-spec">
    <h2>Spécificités</h2>
    <?= $params["product"]["spec"] ?>  
  </div>
  <div class="product-comments">
    <h2>Avis</h2>
    <?php if (count($params["comments"])) { ?>
      <?php foreach ($params["comments"] as $c) { ?>
        <div class="product-comment">
          <p class="product-comment-author">
            <?= $c["firstname"]?> <?= $c["lastname"] ?>
          </p>
          <p>
            <?= $c["content"] ?>
          </p>
        </div>
      <?php } ?>
    <?php } else { ?>
      <p>
        Il n'y a pas d'avis sur ce produit.
      </p>
    <?php } ?>
    <form action="/comment" method="post">
      <input type="text" name="content" placeholder="Rédiger un commentaire" />
      <input type="hidden" name="product" value="<?= $params["product"]["id"] ?>" />
    </form>
  </div>
</div>

</div>

<script src="/public/scripts/product.js"></script>