<?php
  if (isset($_GET["status"])) {
    if ($_GET["status"] == 'login_fail') {
      echo '<div style="margin:32px 32px 0" class="box error">La connexion a échoué. Vérifie tes identifiants et réessaye.</div>';
    }
    if ($_GET["status"] == 'signin_success') {
      echo '<div style="margin:32px 32px 0" class="box info">Inscription réussie ! Tu peux dès à présent te connecter.</div>';
    }
    if ($_GET["status"] == 'logout') {
      echo '<div style="margin:32px 32px 0" class="box info">Tu es déconnecté. À bientôt !</div>';
    }
  }
?>

<div id="account">

<form class="account-login" method="post" action="/account/login">

  <h2>Connexion</h2>
  <h3>Tu as déjà un compte ?</h3>

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <input type="submit" value="Connexion" />

</form>

<form class="account-signin" method="post" action="/account/signin">

  <h2>Inscription</h2>
  <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

  <p>Nom</p>
  <input id="userlastname" type="text" name="userlastname" placeholder="Nom" />

  <p>Prénom</p>
  <input id="userfirstname" type="text" name="userfirstname" placeholder="Prénom" />

  <p>Adresse mail</p>
  <input id="usermail" type="text" name="usermail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input id="userpass1" type="password" name="userpass" placeholder="Mot de passe" />

  <p>Répéter le mot de passe</p>
  <input id="userpass2" type="password" name="userpass" placeholder="Mot de passe" />

  <input type="submit" value="Inscription" />

</form>

</div>

<script src="/public/scripts/signin.js"></script>
