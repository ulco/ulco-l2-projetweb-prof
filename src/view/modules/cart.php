<div id="cart">

<h2>Panier</h2>

<?php if (empty($params["cart"])) { ?>

  <p>Ton panier est vide.</p>

<?php } else { ?>

  <table>
    <?php foreach ($params["cart"] as $product) { ?>
      <tr>
        <td>
          <img src="/public/images/<?= $product["image"] ?>" />
        </td>
        <td>
          <p class="category"><?= $product["category"] ?></p>
          <p class="product" ><?= $product["name"] ?></p>
        </td>
        <td>
          <p class="quantity">Quantité :</p>
          <p>
            <button id="btnM" type="button">-</button>
            <button id="btnQ" type="button"><?= $product["quantity"] ?></button>
            <button id="btnP" type="button">+</button>
          </p>
        </td>
        <td>
          <p>Prix unitaire :</p>
          <p class="price"><?= $product["price"] ?>€</p>
        </td>
      </tr>
    <?php } ?>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td>
        <p>Prix total du panier :</p>
        <p class="total"><?= $params["total"] ?>€</p>
      </td>
    </tr>
  </table>

  <p>
    <a href="#">Procéder au paiement</a>
  </p>

<?php } ?>

</div>