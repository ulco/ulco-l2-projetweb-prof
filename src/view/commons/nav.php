<nav>

  <img src="/public/images/logo.jpeg" />

  <a href="/">Accueil</a>

  <a href="/store">Boutique</a>

  <?php if (isset($_SESSION["user"])) { ?>

    <a href="/account/infos" class="account">
      <img src="/public/images/account.png" />
      <?= $_SESSION["user"]["firstname"] . " " . $_SESSION["user"]["lastname"] ?>
    </a>
    <a href="/cart">Panier</a>
    <a href="/account/logout">Déconnexion</a>

  <?php } else { ?>

    <a href="/account" class="account">
      <img src="/public/images/account.png" />
      Compte
    </a>

  <?php } ?>

</nav>