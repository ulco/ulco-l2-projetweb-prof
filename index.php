<?php

session_start();

/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');

// GET "/store"
$router->get('/store', 'controller\StoreController@store');

// POST "/store"
$router->post('/store', 'controller\StoreController@search');

// GET "/store/{:num}"
$router->get('/store/{:num}', 'controller\StoreController@product');

// GET "/account
$router->get('/account', 'controller\AccountController@account');

// GET "/account/infos
$router->get('/account/infos', 'controller\AccountController@infos');

// POST "/account/signin"
$router->post('/account/signin', 'controller\AccountController@signin');

// POST "/account/login"
$router->post('/account/login', 'controller\AccountController@login');

// POST "/account/update"
$router->post('/account/update', 'controller\AccountController@update');

// GET "/account/logout"
$router->get('/account/logout', 'controller\AccountController@logout');

// GET "/cart"
$router->get('/cart', 'controller\CartController@cart');

// POST "/cart/update"
$router->post('/cart/update', 'controller\CartController@update');

// POST "/comment"
$router->post('/comment', 'controller\CommentController@comment');

// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
