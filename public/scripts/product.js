overQ.style.display = 'none';

document.querySelectorAll('.product-miniatures img').forEach(img => {
  img.onclick = () => {
    splash.src = img.src;
  }
});

btnM.onclick = () => {
  const v = parseInt(btnQ.innerText);
  const vnew = v <= 1 ? 1 : v - 1;
  overQ.style.display = 'none';
  btnQ.innerText = vnew;
  btnQ2.value = vnew;
}

btnP.onclick = () => {
  const v = parseInt(btnQ.innerText);
  const vnew = v >= 5 ? 5 : v + 1;
  if (vnew >= 5) {
    overQ.style.display = 'inline-block';
  }
  btnQ.innerText = vnew;
  btnQ2.value = vnew;
}